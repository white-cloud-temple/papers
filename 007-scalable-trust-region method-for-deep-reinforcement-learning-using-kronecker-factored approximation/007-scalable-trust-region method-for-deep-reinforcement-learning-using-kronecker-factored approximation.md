# 7. Scalable trust-region method for deep reinforcement learning using Kronecker-factored approximation
Wu Y, Mansimov E, Liao S, et al. Scalable trust-region method for deep reinforcement learning using Kronecker-factored approximation[J]. arXiv: Learning, 2017.


### 资料
1. [论文在线阅读/下载地址](https://gitee.com/white-cloud-temple/papers/raw/master/007-scalable-trust-region%20method-for-deep-reinforcement-learning-using-kronecker-factored%20approximation/007-scalable-trust-region%20method-for-deep-reinforcement-learning-using-kronecker-factored%20approximation.pdf)

