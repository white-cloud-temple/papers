# 6. U-Net: Convolutional Networks for Biomedical Image Segmentation
Ronneberger O , Fischer P , Brox T . U-Net: Convolutional Networks for Biomedical Image Segmentation[J]. 2015.


### 资料
1. [论文在线阅读/下载地址](https://gitee.com/white-cloud-temple/papers/raw/master/006-u-net-convolutional-networks-for-biomedical-image-segmentation/006-u-net-convolutional-networks-for-biomedical-image-segmentation.pdf)