# 1. Deep Learning
Lecun Y, Bengio Y, Hinton G. Deep learning.[J]. 2015, 521(7553):436.


### 资料
1. [论文在线阅读/下载地址](https://gitee.com/white-cloud-temple/papers/raw/master/001-deep-learning/001-nature-deep-review.pdf)
2. [论文中文翻译参考](https://artificial-intelligence.net.cn/2019/06/05/1559711811/)
3. [论文课程参考](https://edu.51cto.com/course/21053.html)


### 打卡
|姓名|笔记|
|---|---|
|示例|[示例]()|