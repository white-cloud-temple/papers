# 8. Optimizing Neural Networks with Kronecker-factored Approximate Curvature
Martens J, Grosse R. Optimizing Neural Networks with Kronecker-factored Approximate Curvature[J]. arXiv: Learning, 2015.


### 资料
1. [论文在线阅读/下载地址](https://gitee.com/white-cloud-temple/papers/raw/master/008-optimizing-neural-networks-with-kronecker-factored-approximate-curvature/008-optimizing-neural-networks-with-kronecker-factored-approximate-curvature.pdf)