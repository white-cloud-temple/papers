# 白云观论文学习小组——积少成多计划


### 介绍
白云观论文学习小组为学习论文而设，本小组暂时仅阅读简单的经典论文。  
每周一篇，参与阅读学习本篇论文的进群，不参与的退群（不接受任何不认识的人进群）。  
学习分为论文阅读、开源代码运行、实战调参（小实战）三个阶段。  
每轮学习产出笔记（字数不限）、实战结果截图、论文复现（可选）即为打卡成功，计入白云观论文学习榜。  
每周群内会分享论文相关资料。  


### 当前进度
第一周：阅读三巨头经典论文《Deep Learning》（实验周）  
学习时间：2月17日-2月23日  
打卡内容：只提供笔记即可  
*[点击前往详情页面](https://gitee.com/white-cloud-temple/papers/blob/master/001-deep-learning/001-deep-learning.md)*


### 论文学习列表
1. **[Deep Learning](https://gitee.com/white-cloud-temple/papers/blob/master/001-deep-learning/001-deep-learning.md)** 
Lecun Y, Bengio Y, Hinton G. Deep learning.[J]. 2015, 521(7553):436.
2. **[ImageNet Classiﬁcation with Deep Convolutional Neural Networks](https://gitee.com/white-cloud-temple/papers/blob/master/002-imagenet-classification-with-deep-convolutional-neural-networks/002-imagenet-classification-with-deep-convolutional-neural-networks.md)** Alex Krizhevsky, I Sutskever, G Hinton. ImageNet Classification with Deep Convolutional Neural Networks[C]// International Conference on Neural Information Processing Systems. Curran Associates Inc. 2012.
3. **[A Neural Probabilistic Language Model](https://gitee.com/white-cloud-temple/papers/blob/master/003-a-neural-probabilistic-language-model/003-a-neural-probabilistic-language-model.md)** Bengio Y, Ducharme R, Vincent P, et al. A neural probabilistic language model[J]. Journal of Machine Learning Research, 2003, 3(6): 1137-1155.
4. **[Prioritized Experience Replay](https://gitee.com/white-cloud-temple/papers/blob/master/004-prioritized-experience-replay/004-prioritized-experience-replay.md)** Schaul T, Quan J, Antonoglou I, et al. Prioritized Experience Replay[J]. arXiv: Learning, 2015.
5. **[Playing Atari with Deep Reinforcement Learning](https://gitee.com/white-cloud-temple/papers/blob/master/005-Classical-DQN/Classical-DQN.md)** Mnih V, Kavukcuoglu K, Silver D, et al. Playing Atari with Deep Reinforcement Learning[J]. arXiv: Learning, 2013.
6. **[U-Net: Convolutional Networks for Biomedical Image Segmentation](https://gitee.com/white-cloud-temple/papers/blob/master/006-u-net-convolutional-networks-for-biomedical-image-segmentation/006-u-net-convolutional-networks-for-biomedical-image-segmentation.md)**  Ronneberger O , Fischer P , Brox T . U-Net: Convolutional Networks for Biomedical Image Segmentation[J]. 2015.
    