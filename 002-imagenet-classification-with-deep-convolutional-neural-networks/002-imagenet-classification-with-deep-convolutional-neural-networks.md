# 2. ImageNet Classiﬁcation with Deep Convolutional Neural Networks
Alex Krizhevsky, I Sutskever, G Hinton. ImageNet Classification with Deep Convolutional Neural Networks[C]// International Conference on Neural Information Processing Systems. Curran Associates Inc. 2012.


### 资料
1. [论文在线阅读/下载地址](https://gitee.com/white-cloud-temple/papers/raw/master/002-imagenet-classification-with-deep-convolutional-neural-networks/002-imagenet-classification-with-deep-convolutional-neural-networks.pdf)