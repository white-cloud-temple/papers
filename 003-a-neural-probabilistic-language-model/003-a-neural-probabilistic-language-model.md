# 3. A Neural Probabilistic Language Model
Bengio Y, Ducharme R, Vincent P, et al. A neural probabilistic language model[J]. Journal of Machine Learning Research, 2003, 3(6): 1137-1155.


### 资料
1. [论文在线阅读/下载地址](https://gitee.com/white-cloud-temple/papers/raw/master/003-a-neural-probabilistic-language-model/003-a-neural-probabilistic-language-model.pdf)